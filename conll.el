;;; conll.el --- A Major Mode for annotating CoNLL files        -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arthur Amalvy

;; Author: Arthur Amalvy
;; Keywords: languages
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'cl-lib)


;;; Global variables

(defvar conll-class-colors
  '("cyan" "green" "brown" "saddle brown" "goldenrod" "magenta" "steel blue" "red" "hot pink" "purple")
  "A list of possible colors to associate to each NER class, used when automatically determining a colorscheme.")

(defconst conll-scheme-classic
  '(("PER" . "brown")
    ("LOC" . "cyan")
    ("ORG" . "saddle brown")
    ("MISC" . "green")))

(defconst conll-scheme-novelties
  '(("CHR" . "brown")
    ("LOC" . "cyan")
    ("ORG" . "saddle brown")
    ("GRP" . "goldenrod")
    ("MSC" . "green")))

(defvar conll-scheme nil
  "An alist, with elements of the form (NER-CLASS . COLOR). If
`nil', the scheme will be automatically determined when
annotating a file.")

(defvar conll-metadata-interpretation-scheme nil
  "Either `nil' for no interpretation, or `novelties'. If non `nil',
 will smartly ask for some metadata values when calling
`conll-write-buffer'. See `conll-interpret-metadata'.")


;;; Metadata
;; conll.el supports Novelties style metadata. Metadata is at the
;; beginning of the file, each metadata line is of the form:
;; # key: value

(defun conll-interpret-metadata (metadata)
  "Interpret METADATA according to
`conll-metadata-interpretation-scheme'."
  (cond
   ((eq conll-metadata-interpretation-scheme 'novelties)
    (progn
      (when (not (alist-get "title" metadata nil nil #'string=))
	(setf (alist-get "title" metadata nil nil #'string=) (read-string "title: ")))
      (when (not (alist-get "chapter" metadata nil nil #'string=))
	(setf (alist-get "chapter" metadata nil nil #'string=) (read-string "chapter: ")))
      (setf (alist-get "annotator" metadata nil nil #'string=)
	    (read-string "annotator: " (alist-get "annotator" metadata "" nil #'string=)))
      (setf (alist-get "guidelines-version" metadata nil nil #'string=)
	    (read-string "guidelines-version: " (alist-get "guidelines-version" metadata "" nil #'string=)))
      (setf (alist-get "last-updated" metadata nil nil #'string=)
	    (read-string "last-updated: " (format-time-string "%d-%m-%Y")))
      (setf (alist-get "comment" metadata nil nil #'string=)
	    (read-string "comment: " (alist-get "comment" metadata "" nil #'string=)))
      (let ((sorting-order
	     '("title" "chapter" "annotator" "guidelines-version" "last-updated" "comment")))
	(sort metadata (lambda (kv1 kv2)
			 (< (or (cl-position (car kv1) sorting-order :test #'string=)
				(length sorting-order))
			    (or (cl-position (car kv2) sorting-order :test #'string=)
				(length sorting-order))))))))
   (t metadata)))


;;; General

(defun conll-flatten-sentences (sentences)
  "Flatten SENTENCE into tokens-and-tags."
  (let ((tokens-and-tags (list)))
    (dolist (sent sentences)
      (dolist (token-and-tag sent)
	(push token-and-tag tokens-and-tags)))
    (reverse tokens-and-tags)))

(cl-defun conll-parse-buffer (&optional (separator " "))
  "Parse the current conll buffer."
  (let* ((content (buffer-string))
	 (lines (seq-filter (lambda (line) (not (string= line ""))) (split-string content "\n")))
	 (metadata-rx (rx "# "
			  ;; key
			  (group (one-or-more (not ":")))
			  ": "
			  ;; value
			  (group (zero-or-more anychar)))))
    (cons
     ;; metadata
     (cl-loop for line in lines
	      until (not (string-match metadata-rx line))
	      collect (cons (match-string 1 line) (match-string 2 line)))
     ;; conll lines
     (cl-loop for line in lines
	      when (not (string-match metadata-rx line))
	      collect (let ((splitted (split-string line separator)))
			(cons (car splitted) (cadr splitted)))))))

(cl-defun conll-parse-file (file &optional (separator " "))
  "Parse FILE"
  (save-excursion
    (find-file file)
    (conll-parse-buffer separator)))

(defun conll-write-buffer (tokens-and-tags &optional metadata)
  "Write TOKENS-AND-TAGS in conll format to the current buffer. If
METADATA is supplied, also write Novelties-style metadata to the
beggining of the buffer."
  (erase-buffer)
  (goto-char (point-min))
  (let ((interpretated-metadata (conll-interpret-metadata metadata)))
    (when interpretated-metadata
      (cl-loop for (key . value) in interpretated-metadata
	       do (insert (format "# %s: %s\n" key value)))
      (insert "\n")))
  (cl-loop for (token . tag) in tokens-and-tags
	   do (insert (format "%s %s\n" token tag))))

(defun conll-write-file (tokens-and-tags file &optional metadata)
  "write TOKENS-AND-TAGS in conll format to the given FILE. If
METADATA is supplied, also write Novelties-style metadata to the
beggining of the file."
  (save-excursion
    (find-file file)
    (conll-write-buffer tokens-and-tags)
    (save-buffer)))

(defun conll-class-from-tag (tag)
  (cond
   ((string= tag "O") "O")
   (t (substring tag 2))))

(defun conll-split-into-sentences (tokens-and-tags)
  "Split TOKENS-AND-TAGS into sentences.
sentences are a list of list of tokens and tags."
  (let ((sentences (list))
	(current-sentence (list)))
    (cl-loop for (token . tag) in tokens-and-tags
	     and (next-token . next-tag) in (append (cdr tokens-and-tags) (list (cons nil nil)))
	     do
	     (push (cons token tag) current-sentence)
	     (when (and (not (string= next-token "''"))
			(member token '("''" "." "?" "!")))
	       (push (reverse current-sentence) sentences)
	       (setq current-sentence (list))))
    (reverse sentences)))

(defun conll-detect-classes (tokens-and-tags)
  "Detect all the unique NER classes from TOKENS-AND-TAGS."
  (let ((classes (list)))
    (cl-loop for (token . tag) in tokens-and-tags
	     when (not (string-equal tag "O"))
	     do (cl-pushnew (conll-class-from-tag tag) classes :test 'equal))
    (setq classes (cl-sort classes 'string-lessp :key 'downcase))))


;;; Annotation

(defun conll--annotation-highlight (scheme)
  "Set entity faces in the current annotation buffer.
Faces are set according to colors in SCHEME."
  (save-excursion
    (goto-char (point-min))
    (while (not (eobp))
      (let* ((inhibit-read-only t)
	     (ner-tag (plist-get (text-properties-at (point)) 'ner-tag))
	     (ner-class (when (and ner-tag (> (length ner-tag) 2))
			  (conll-class-from-tag ner-tag)))
	     (color (cdr (assoc ner-class scheme))))
	(if color
	    (let ((face (if (equal (substring ner-tag 0 1) "B")
			    `(bold (:foreground ,color) underline)
			  `(bold (:foreground ,color)))))
	      (put-text-property (point) (1+ (point)) 'face face)
	      (put-text-property (point) (1+ (point)) 'help-echo ner-tag))
	  (remove-text-properties (point) (1+ (point)) '(face help-echo))))
      (forward-char))))

(defun conll--annotation-get-selected-words ()
  "Automatically determine the words selected by the user."
  (if (region-active-p)
      ;; the user is selecting text using a region: returns the bounds
      ;; of words in that region
      (let ((region-beg (region-beginning))
	    (region-e (region-end))
	    (word-bounds (list)))
	(pop-mark)
	(goto-char region-beg)
	(when (looking-at " ")
	  (search-forward " "))
	(while (not (>= (point) region-e))
	  (let ((word-start (point)))
	    (search-forward " ")
	    (push (cons word-start (1- (point))) word-bounds)))
	(reverse word-bounds))
    ;; the user is not selecting text: return the next word the user
    ;; is on a space: go to next word
    (when (looking-at (rx space))
      (search-forward " "))
    ;; return current word bounds
    (let ((word-start (point)))
      (search-forward " ")
      (list (cons word-start (1- (point)))))))

(defun conll-annotation-annotate-ner-class-dwim (scheme)
  "Annotate the currently selected words if region is active, or
the word at point otherwise."
  (interactive)
  (let ((inhibit-read-only t)
	(ner-class (completing-read "ner class: " (mapcar #'car scheme) nil t))
	(selected-word-bounds (conll--annotation-get-selected-words)))
    (put-text-property (car (car selected-word-bounds))
		       (cdr (car selected-word-bounds))
		       'ner-tag
		       (format "B-%s" ner-class))
    (cl-loop for (start . end) in (cdr selected-word-bounds)
	     do (put-text-property start end 'ner-tag (format "I-%s" ner-class))))
  (conll--annotation-highlight scheme))

(defun conll-annotation-annotate-custom-tag-dwim (scheme)
  "Annotate the currently selected words (if region is active) or
the current word with a fully custom tag."
  (interactive)
  (let ((inhibit-read-only t)
	(tag (read-string "custom tag: "))
	(selected-word-bounds (conll--annotation-get-selected-words)))
    (cl-loop for (start . end) in selected-word-bounds
	     do (put-text-property start end 'ner-tag tag)))
  (conll--annotation-highlight scheme))

(defun conll-annotation-delete-ner-class-dwim (scheme)
  "Delete the annotation for the currently selected words if region
is active, or word at point otherwise."
  (interactive)
  (let ((inhibit-read-only t)
	(selected-word-bounds (conll--annotation-get-selected-words)))
    (cl-loop for (start . end) in selected-word-bounds
	     do (put-text-property start end 'ner-tag "O")))
  (conll--annotation-highlight scheme))

(defun conll-annotation-next-potential-entity ()
  "Navigate to the next potential entity token. Return the index of
the found entity, or nil if none was found."
  (interactive)
  (let* ((case-fold-search nil)
	 ;; search for an unannotated capitalised token
	 (unannotated-entity-idx (save-excursion
				   (forward-char)
				   (search-forward-regexp (rx upper) nil t)))
	 ;; search for an annotated entity
	 (annotated-entity-match
	  (save-excursion
	    ;; get out of current entity
	    (when (not (equal (get-text-property (point) 'ner-tag) "O"))
	      (text-property-search-forward 'ner-tag "O" t))
	    ;; goto next annotated entity
	    (text-property-search-forward 'ner-tag "B" (lambda (value prop)
							 (and prop
							      (equal (substring prop 0 1)
								     value))))))
	 (entity-idx 
	  (cond ((and unannotated-entity-idx annotated-entity-match)
		 (min unannotated-entity-idx (1+ (prop-match-beginning annotated-entity-match))))
		(annotated-entity-match (1+ (prop-match-beginning annotated-entity-match)))
		(unannotated-entity-idx unannotated-entity-idx)
		(t nil))))
    (when entity-idx
      (goto-char (- entity-idx 1)))
    entity-idx))

(defun conll-annotation-undo ()
  (interactive)
  (let ((inhibit-read-only t))
    (undo)))

(defvar conll-annotation-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "q") (lambda () (interactive)))
    (cl-loop for key in '("C-x u" "C-/" "C-_" "<undo>")
	     do (define-key map (kbd key) #'conll-annotation-undo))
    (set-keymap-parent map special-mode-map)
    map)
  "Keymap for CoNLL annotation mode.")

(define-derived-mode conll-annotation-mode special-mode "CoNLL"
  "Major mode for annotating CoNLL files.
\\{conll-annotation-mode-map}"
  (conll-dont-be-evil))

(defun conll-annotate-sent (sent scheme)
  "Interactively annotate SENT. SENT is a list of cons (token
. tag))."
  (switch-to-buffer "*conll-annotation*")
  (let ((inhibit-read-only t)
	(sents-nb (length sent)))
    (erase-buffer)
    (cl-loop for (token . tag) in sent
	     and i from 0
	     do (progn (insert (propertize (format "%s" token) 'ner-tag tag))
		       (insert " "))))
  (conll--annotation-highlight scheme)
  (conll-annotation-mode)
  ;; these commands depends on the local SCHEME
  (define-key
   conll-annotation-mode-map
   (kbd "a")
   (lambda ()
     (interactive)
     (conll-annotation-annotate-ner-class-dwim scheme)))
  (define-key
   conll-annotation-mode-map
   (kbd "c")
   (lambda ()
     (interactive)
     (conll-annotation-annotate-custom-tag-dwim scheme)))
  (define-key
   conll-annotation-mode-map
   (kbd "d")
   (lambda ()
     (interactive)
     (conll-annotation-delete-ner-class-dwim scheme)))
  ;; these are defined here because they exit recursive-edit and
  ;; modify USER-CHOICE
  (let ((user-choice 'next))
    ;; go to next entity
    (define-key
     conll-annotation-mode-map
     (kbd "e")
     (lambda ()
       (interactive)
       (unless (conll-annotation-next-potential-entity)
	 (setq user-choice 'next)
	 (exit-recursive-edit))))
    ;; go to previous sent
    (define-key
     conll-annotation-mode-map
     (kbd "p")
     (lambda ()
       (interactive)
       (setq user-choice 'previous)
       (exit-recursive-edit)))
    ;; go to next sent
    (define-key
     conll-annotation-mode-map
     (kbd "n")
     (lambda ()
       (interactive)
       (setq user-choice 'next)
       (exit-recursive-edit)))
    ;; exit session
    (define-key
     conll-annotation-mode-map
     (kbd "C-c C-k")
     (lambda ()
       (interactive)
       (setq user-choice 'exit)
       (exit-recursive-edit)))
    (goto-char (point-min))
    ;; go the the first entity of the sentence
    (let ((case-fold-search nil))
      (when (not (looking-at (rx upper-case)))
	(conll-annotation-next-potential-entity)))
    (recursive-edit)
    (setq sent (mapcar (lambda (propertized-token)
			 (cons (substring-no-properties propertized-token)
			       (get-text-property 0 'ner-tag propertized-token)))
		       (split-string (buffer-string))))
    (list sent user-choice)))

(cl-defun conll-annotate-file (arg conll-file)
  "Interactively annotate CONLL-FILE. if ARG is supplied, ask for
custom separator."
  (interactive "P\nffind CoNLL file: ")
  (let* ((separator (if arg (read-string "separator: ") " "))
	 (parsed-buffer (conll-parse-file conll-file separator))
	 (metadata (car parsed-buffer))
	 (tokens-and-tags (cdr parsed-buffer))
	 (sentences (conll-split-into-sentences tokens-and-tags))
	 (sentences-vector (vconcat sentences))
	 ;; automatically detect scheme if CONLL-SCHEME is not set
	 (scheme (or conll-scheme
		     (cl-loop for cls in (conll-detect-classes tokens-and-tags)
			      and j from 0
			      collect (cons cls (nth j conll-class-colors)))))
	 (i 0)
	 (last-user-choice nil)
	 ;; allows tooltip in the echo area - useful for displaying ner
	 ;; tags in the echo area
	 (help-at-pt-display-when-idle t))
    ;; enable help-at-pt-display-when-idle
    (help-at-pt-cancel-timer)
    (help-at-pt-set-timer)
    ;; Annotation loop
    (while (and (not (eq 'exit last-user-choice))
		(< i (length sentences-vector)))
      (message (format "sentence %i/%i" (1+ i) (length sentences-vector)))
      (cl-destructuring-bind
	  (sent user-choice)
	  (conll-annotate-sent (aref sentences-vector i) scheme)
	(setq last-user-choice user-choice)
	(aset sentences-vector i sent)
	(cond
	 ((eq 'next user-choice)
	  (setq i (1+ i)))
	 ((eq 'previous user-choice)
	  (when (> i 0)
	    (setq i (1- i))))
	 ((eq 'exit user-choice) nil))))
    ;; End of annotation: present the user with a buffer containing
    ;; its annotation under the conll format
    (let ((inhibit-read-only t)
	  (output-buffer-name (format "%s.annotated" conll-file)))
      (switch-to-buffer output-buffer-name)
      (conll-write-buffer (conll-flatten-sentences (append sentences-vector nil)) metadata)
      (write-file output-buffer-name))))


;;; Evil "Compatibility"

(defun conll-dont-be-evil ()
  (when (and (fboundp #'evil-mode) evil-mode)
    (add-to-list 'evil-emacs-state-modes 'conll-annotation-mode)))


(provide 'conll)
;;; conll.el ends here
