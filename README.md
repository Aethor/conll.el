# conll.el

Annotate CoNLL-2003 formatted files in Emacs, *fast*.

| Command                                    | Keybinding                         |
|--------------------------------------------|------------------------------------|
| Start annotating file                      | <kbd>M-x conll-annotate-file</kbd> |
| Annotate word or selection                 | <kbd>a</kbd>                       |
| Annotate word or selection with custom tag | <kbd>c</kbd>                       |
| Delete annotation for word or selection    | <kbd>d</kbd>                       |
| Go to next possible entity                 | <kbd>e</kbd>                       |
| Go to previous sentence                    | <kbd>p</kbd>                       |
| Go to next sentence                        | <kbd>n<kbd>                        |
| Exit session now (and save)                | <kbd>C-c C-k<kbd>                  |

The following configuration variables can be set:

- `conll-scheme`: the annotation scheme to use. Set it to `conll-scheme-classic` (the classic CoNLL-2003 scheme with 4 classes), `conll-scheme-novelties` (support for the [Novelties](https://github.com/CompNet/Novelties) corpus) or to a custom alist with elements of the form `(NER-CLASS . COLOR)`. If `nil` (the default), try to automatically determine the classes at runtime and assign colors to them automatically.
- `conll-metadata-interpretation-scheme`: how to intepret metadata. If `nil` (the default), no interpretation. If `novelties`,  smartly ask for [Novelties](https://github.com/CompNet/Novelties) metadata values when saving.
